# Natalia Canada

I am a software developer based in the Philippines with 2 years of experience in software industry.

My focus area is in mobile development and I have been using the Flutter Framework for quite some time now. Before that, I was using React Native for small learning projects. I first learned android development during college and it made me very interested in learning and pursuing a mobile development career.

# Contact Information

LinkedIn: [http://linkedin.com/in/natalia-canada](http://linkedin.com/in/natalia-canada-399519218)

Website Portfolio: [https://canada-flutter-portfolio.herokuapp.com/#/](https://canada-flutter-portfolio.herokuapp.com/#/)

# Work Experience

## Flutter Developer Trainee

FFUF Manila Inc., from July 2021 - present

- I am currently a trainee focusing on mobile application development using Flutter.
- This also involves DevOps, SCRUM and tools necessary in software development.

## RPA Developer

Indra Philippines Inc., from September 2019 to April 2021

- I have worked with traditional robots for Data Entry, Validation and Verification, and Desktop Automations that allows repetitive tasks to be accomplished with significant reduced amount of time.
- This also involved languages such as R to explore the Excel automations and Python for UiPath Orchestrator API.

# Skills

## Technical Skills

- Programming
- Design
- Software Engineering
- Mobile Application Development

## Soft Skills

- Team Player
- Communication
- Open-Mindedness
- Problem-Solver

# Education

## BS Computer Engineering

Technological Institute of the Philippines QC, 2014-2019